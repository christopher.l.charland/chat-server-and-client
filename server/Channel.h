//
// Created by charizard on 9/6/19.
//

#ifndef SERVER_CHANNEL_H
#define SERVER_CHANNEL_H

/** @file */

#include "t_vec.h"

#include "User.h"

/** Maximum length for a channel name */
#define CHANNEL_NAME_LEN 16

/** Maximum length for the Message Of The Day */
#define MOTD_LEN 512

typedef struct user_info user_info;

typedef struct channels_list
{
    struct channel *channelsll; /** A Linked List of channels */
} channels_list;

typedef struct channel_info
{
    char name[CHANNEL_NAME_LEN];
    char *motd;
    T_Vec *users;
} channel_info;

// be careful when using this, the *user also has a next

/** @struct chuser
 *
 * @var user
 *  Pointer to the channel user data
 *
 * @var next
 *  Pointer to the next user in this channel
 *
 * @typdef chuser chuser
 *  document typedef
 */

/** @struct channel
 *
 * @var num_users
 *  Number of users in the channel
 *
 * @var name
 *  Name of the channel
 */
typedef struct channel
{
    int            num_users;
    char           name[CHANNEL_NAME_LEN];
    char           *motd;
    struct chuser  *chusers;
    struct channel *next;

} channel;

/**
 * Create a new list of channels
 * @return newly allocated channels_list
 */
channels_list *
Channel_init();

/**
 * Frees all resources associated with a channels_list
 * @param channels channels_list to destroy
 * @return 0 on success, 1 on failure
 */
int
Channel_destroy(channels_list *channels);

/**
 * Adds a newly allocated channel to the channels_list passed
 * @param channelName name of the new channel
 * @param channels
 * @return
 */
int
Channel_create(char *channelName, T_Vec *channels);

/**
 * Adds a user to a specific named channel. This function adds n_user
 * to a linked list of users for the channel.  If the channel does not
 * exist, this function will create it.
 * @param channelName The name of the channel to join
 * @param channels The list of channels
 * @param n_user The user to add
 * @return
 */
int
Channel_join(char *channelName, T_Vec *channels, user_info *n_user);

#endif //SERVER_CHANNEL_H
