/** @mainpage
 * @detail
 * SHIRC is a chat suite based on IRC protocol as described in RFC 1459
 * but drastically simplified. This module implements the SHIRC (SHitty IRC)
 * server in its entirety
 * @version 0.1.0
 * @author Christopher Charland
 * @date October 1, 2019
 *
 *
 * @copyright
 * Copyright (c) 2019 Christopher Charland
 * \n\n
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * \n\n
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * \n\n
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include <netdb.h>
#include <sys/socket.h>
#include <getopt.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <poll.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <time.h>

#include "User.h"
#include "Pool.h"
#include "Channel.h"
#include "t_vec.h"

// Who wants to keep typing struct over and over?
typedef struct pollfd pollfd;

const size_t MSG_SIZE = 512;

typedef struct global_state
{
    int sd;
    T_Vec *users;
    T_Vec *channels;
    T_Vec *pollfds;
    Pool *workers;
    pthread_mutex_t mx;
} global_state;

typedef struct handle_message_args
{
    global_state *state;
    int incoming_socket;

} handle_message_args;


typedef struct change_nick_args
{
    global_state *state;
    user_info *source_user;
    char new_nick[USERNAME_LEN];
} change_nick_args;

typedef struct message_info
{
    uint8_t type;
    uint8_t subtype;
    char *target; //16 currently, THIS MUST BE THE SAME AS USERNAME_LEN
    char *message;

} message_info;

typedef struct distribute_msg_args
{
    global_state *state;
    message_info msg; //lololololo .s
} distribute_msg_args;

typedef struct optn
{
    char port[8];
    char *path;
} optn;

typedef struct notify_nick_change_args
{
    global_state *state;
    user_info *source_user;
    char old_nick[USERNAME_LEN];
    char new_nick[USERNAME_LEN];
} notify_nick_change_args;

void *
distribute_msg(void *arg);

void *
change_nick(void *arg);

int
get_pollfd_by_fd(void* arg_pollfd, void* arg_fd)
{
    pollfd *cmp_pollfd = arg_pollfd;
    int cmp_fd = (int)arg_fd;

    printf("FDs: %d %d\n", cmp_pollfd->fd, cmp_fd);

    if (cmp_pollfd->fd == cmp_fd)
    {
        //printf("Foundit");
        return 0;
    }
    return 1;

}

int
get_user_by_fd(void* arg_user, void* arg_fd)
{
    user_info *cmp_user = arg_user;
    int cmp_fd = (int)arg_fd;
    if (cmp_user->sock_fd == cmp_fd)
    {
        return 0;
    }
    return 1;
}

//int
//get_user_by_name(void* arg_user, void *arg_name)
//{
//    user_info *user = arg_user;
//    char *name = arg_name;
//    return strncmp(user->username, name, USERNAME_LEN);
//}
//
//int
//get_channel_by_name(void *arg_channel, void *arg_channel_name)
//{
//    channel_info *channel = arg_channel;
//    char *channel_name = arg_channel_name;
//    return strncmp(channel->name, channel_name, CHANNEL_NAME_LEN);
//}

int
notify_nick_change(void *arg_channel, void *arg_nicks)
{
    channel_info *channel = arg_channel;
    notify_nick_change_args *nicks = arg_nicks;
    printf("Notifying channel of nick change: %s\n", channel->name);
    distribute_msg_args dm_args;
    dm_args.state = nicks->state;
    printf("About to call calloc.\n");
    dm_args.msg.message = calloc(1, MSG_SIZE);
    if (dm_args.msg.message == NULL)
    {
        perror("Failed to allocate dm_args.msg.message in notify_nick_change.");
        return 1;
    }

    dm_args.msg.target = calloc(1, CHANNEL_NAME_LEN);
    if (dm_args.msg.target == NULL)
    {
        perror("Failed to allocate dm_args.msg.target in notify_nick_change.");
        return 1;
    }
    printf("About to snprintf\n");

    snprintf(dm_args.msg.message, MSG_SIZE, "SERVER %s %s", nicks->old_nick, nicks->new_nick);
    printf("About to strncpy:%s: %p\n", channel->name, dm_args.msg.target);
    strncpy(dm_args.msg.target, channel->name, CHANNEL_NAME_LEN);
    dm_args.msg.type = 2;
    dm_args.msg.subtype = 2;
    printf("About to call distribute_msg.\n");
    distribute_msg(&dm_args);
    free(dm_args.msg.message); //Why is this here
    printf("End of notify\n");
    return 0;
}

// Thread killing stuff pretty much stolen from
// https://stackoverflow.com/questions/7961029/how-can-i-kill-a-pthread-that-is-in-an-infinite-loop-from-outside-that-loop
// I'm not quite sure why I didnt simply make this an int
int
quit_signal(pthread_mutex_t *mutex)
{
    //printf("quit_signal called\n");
    switch (pthread_mutex_trylock(mutex))  // Returns EBUSY when its locked
    {
        case 0:
            pthread_mutex_unlock(mutex); // If we got the lock, we want to unlock it so other threads can die
            printf("quit_signal returning 1.\n");
            return 1; // 1 is the kill signal

        case EBUSY:
            return 0;
    }
    return 1;
}

void *
accept_thread(void *arg)
{
    //printf("accept_thread running.\n");
    global_state *state = arg;

    // set up single element array of pollfd's for poll
    struct pollfd accept_pollfd;
    accept_pollfd.fd = state->sd;
    accept_pollfd.events = POLLIN;

    while (!quit_signal(&state->mx))
    {
        //printf("accept_thread inside main loop.\n");
        struct sockaddr_storage client;
        char ipstr[INET6_ADDRSTRLEN];
        socklen_t client_sz = sizeof(client);

        int pollret = poll(&accept_pollfd, 1, 100);
        if (pollret == 0)
        {
            continue;
        }

        int remote = accept(state->sd, (struct sockaddr *)&client, &client_sz);
        if (remote < 0)
        {
            perror("Could not accept remote");
            continue;
        }
        else
        {
            // deal with both IPv4 and IPv6:
            int port;
            if (client.ss_family == AF_INET)
            {
                struct sockaddr_in *s = (struct sockaddr_in *)&client;
                port = ntohs(s->sin_port);
                inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
            }
            else
            { // AF_INET6
                struct sockaddr_in6 *s = (struct sockaddr_in6 *)&client;
                port = ntohs(s->sin6_port);
                inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
            }
            printf("Connection received: %s:%d\n", ipstr, port);

            // Without the O_NONBLOCK flag the workers will hang waiting for
            // data when poll says theres something to do on one of the fd's
            // This happens when a client exits without sending the disconnect command.
            fcntl(remote, O_NONBLOCK);

            // Add connection to client list, username will be updated when the users runs /nick
            User_add("UNKNOWN_USER", state->users, state->pollfds, remote);
        }
    }
    printf("accept_thread dying\n");
    return NULL;
}


void *
handle_msg(void *arg)
{

    // TODO: figure out if this needs to be MSG_SIZE + 1 so no segfaults calling strlen
    // on the buffer
    char *buffer = calloc(1, MSG_SIZE);
    if (buffer == NULL)
    {
        perror("Failed to allocate buffer in handle_msg");
        goto RETURN;
    }
    handle_message_args *params = arg;

    // Get the pollfd for this fd and reset its events
    pollfd *pollfd_from_T_Vec = T_Vec_find_ref(params->state->pollfds,
                                               (void*)params->incoming_socket,
                                               get_pollfd_by_fd);
    printf("%p\n", pollfd_from_T_Vec);
    pollfd_from_T_Vec->events = POLLIN;

    // Get the index of the user
    ssize_t user_idx = T_Vec_find_idx(params->state->users,
                                     (void*)params->incoming_socket,
                                     get_user_by_fd);
    printf("%li\n", user_idx);

    // Get a pointer to the user
    user_info *source_user = T_Vec_fetch_ref(params->state->users, user_idx);

    // receive the data into 512 byte buffer allocated at start
    ssize_t received = recv(params->incoming_socket, buffer, MSG_SIZE, 0);
    printf("Recieved %ld bytes: %s\n", received, buffer + 2);
    if (received == 0)
    {
        printf("Remote end unexpectedly closed connection.\n");
        close(params->incoming_socket);
        ssize_t pollfd_idx = T_Vec_find_idx(params->state->pollfds, (void*)pollfd_from_T_Vec->fd, get_pollfd_by_fd);

        User_remove(params->state->users, params->state->pollfds, params->state->channels, user_idx, pollfd_idx);
        goto FREE_BUFF;
    }

    // Print command type and subtype
    printf("command type: %hhu:%hhu\n", (uint8_t)*buffer, (uint8_t)(*(buffer + 1)));

    //int cmd_result = Commands_parse(buffer);
    char *msg_start = buffer + 2;
    char *channel_name = calloc(1, CHANNEL_NAME_LEN);
    if (channel_name == NULL)
    {
        perror("Failed to allocate channel_name in handle_msg");
        goto FREE_BUFF;
    }
    char *space = NULL;
    space = strchr(msg_start, ' ');
    switch ((uint8_t)*buffer)
    {
        case 1:
            ;
            printf("PRIVMSG %s\n", msg_start);
            if ((space - msg_start) > CHANNEL_NAME_LEN)
            {
                printf("Invalid channel name (too long).\n");
                goto FREE_ALL;
            }
            else
            {
                //char *channel_dist = calloc(1, CHANNEL_NAME_LEN);
                strncpy(channel_name, msg_start, space - msg_start);
            }

            // create message distribution parameters
            distribute_msg_args d_params;
            // copy the message into new buffer
            char *message_dist = calloc(1, MSG_SIZE);
            if (message_dist == NULL)
            {
                perror("Failed to allocate message_dist in handle_msg");
                goto FREE_ALL;
            }
            snprintf(message_dist, MSG_SIZE, "%s %s", source_user->username, space + 1);
            //strncpy(message_dist, space + 1, MSG_SIZE);
            d_params.state = params->state;
            d_params.msg.type = 1;
            d_params.msg.subtype = 1;
            d_params.msg.target = channel_name;
            d_params.msg.message = message_dist;

            // find the channel
            T_Vec_find_ref(source_user->channels, channel_name, get_channel_by_name);

            //TODO: refactor this out into a pool queue item
            distribute_msg(&d_params);
            free(message_dist);
            goto FREE_ALL;

        // Change nick
        case 2:
            ;
            printf("User requests nick change.\n");
            change_nick_args nick_params;

            nick_params.state = params->state;
            nick_params.source_user = source_user;

            strncpy(nick_params.new_nick, msg_start, USERNAME_LEN);

            // Maybe this should be part of User.c/h but here it is for now
            change_nick(&nick_params);
            goto FREE_ALL;

        // Join a Channel
        case 4:
            printf("JOIN %s\n", msg_start);
            //space = strchr(msg_start, ' ');
            if (strlen(msg_start) > CHANNEL_NAME_LEN)
            {
                printf("Invalid channel name (too long).\n");
                goto FREE_ALL;
            }
            else
            {
                strncpy(channel_name, msg_start, CHANNEL_NAME_LEN);
            }
            Channel_join(channel_name, params->state->channels, source_user);
            goto FREE_ALL;

        // Send a file
        case 9:
            printf("SEND %s\n", msg_start);

            user_info *target_user = T_Vec_find_ref(params->state->users, msg_start, get_user_by_name);
            if (target_user == NULL)
            {
                perror("Unable to find target user for send.");
                return NULL;
            }

            // Send that user a notification
            // space+1 is the start of the path of the file to be transfered
            char *message =  calloc(1, MSG_SIZE);
            // 9 is sendfile, subcmd 2 is incoming request
            snprintf(message, MSG_SIZE, "%c%c%s", 9, 2, space + 1);
            send(target_user->sock_fd, message, strlen(message), 0);

            free(message);
            goto FREE_ALL;

        case 24:
            pthread_mutex_unlock(&params->state->mx);
            break;

        default:
            printf("Received unknown command.\n");
            goto FREE_ALL;
    }

    FREE_ALL:
    free(channel_name);

    FREE_BUFF:
    free(buffer);

    RETURN:
    free(params);
    return NULL;
}

void *
change_nick(void *arg)
{
    change_nick_args *params = arg;

    notify_nick_change_args nicks;
    nicks.state = params->state;

    strncpy(nicks.old_nick, params->source_user->username, USERNAME_LEN);

    strncpy(nicks.new_nick, params->new_nick, USERNAME_LEN);

    snprintf(params->source_user->username, USERNAME_LEN, "%s", params->new_nick);

    T_Vec_map(params->source_user->channels, (void*)&nicks, notify_nick_change);

    return NULL;
}

int
send_to_user(void *arg_user, void *arg_send_buffer)
{
    user_info *user = arg_user;
    char *send_buffer = arg_send_buffer;
    printf("Sending message to: %s\n", user->username);
    send(user->sock_fd, send_buffer, strlen(send_buffer), 0);
    return 0;
}



void *
distribute_msg(void *arg)
{
    distribute_msg_args *params = arg;
    char *message = calloc(1, MSG_SIZE);
    if (message == NULL)
    {
        perror("Failed to allocate message in distribute_msg.");
        return NULL;
    }

    // Build the message buffer
    snprintf(message,
             MSG_SIZE,
             "%c%c%s %s",
             params->msg.type,
             params->msg.subtype,
             params->msg.target,
             params->msg.message);
    //printf("%s\n", params->message.message);

    // Build the message to distribute
    if (params->msg.target[0] == '#')
    {
        //printf("Distributing to channel: %s\n", params->message.target);
        //printf("Distributing message: %s\n", params->message.message);
        channel_info *channel_to_send = T_Vec_find_ref(params->state->channels, params->msg.target, get_channel_by_name);
        int map_ret = T_Vec_map(channel_to_send->users, message, send_to_user);
        printf("map_ret: %d\n", map_ret);
    }
    else
    {
        user_info *user = T_Vec_find_ref(params->state->users, params->msg.target, get_user_by_name);
        if (user == NULL)
        {
            printf("Unknown user: %s\n", params->msg.target);
            return NULL;
        }
        printf("Sending to user: %s\n", params->msg.target);
        send_to_user(user, message);
    }


    free(message);
    return NULL;
}

void *
notifier_thread(void *arg)
{
    // Cast *arg to global_state
    global_state *state = arg;

    while (!quit_signal(&state->mx))
    {
        // If there are no clients to poll, sleep for 100ms then continue.
        if (T_Vec_len(state->pollfds) == 0)
        {
            struct timespec ts;
            ts.tv_sec = 0;
            ts.tv_nsec = 100000;
            nanosleep(&ts, NULL);
            continue;
        }

        // Poll all the clients for activity on their sockets.
        int numtasks = poll(T_Vec_start(state->pollfds), T_Vec_len(state->pollfds), 2000);
        //printf("%d\n", numtasks);
        // If poll returned negative there was an error.
        if (numtasks < 0)
        {
            perror("Poll Error");
            continue;
        }

        // If there are sockets with activity
        if (numtasks > 0)
        {
            // Iterate over array of pollfd's and check the returned
            // events field that poll filled out for us.
            for (size_t x = 0; x < T_Vec_len(state->pollfds); ++x)
            {
                // TODO: check the other POLL possibilities
                // TODO: https://stackoverflow.com/questions/17692447/does-poll-system-call-know-if-remote-socket-closed-or-disconnected
                // Pull out returned events for a socket
                pollfd *curr_pollfd = T_Vec_fetch_ref(state->pollfds, x);
                unsigned short test = curr_pollfd->revents;
                // If HUP or ERR bit set
                if ((test & (unsigned)POLLHUP) | (test & (unsigned)POLLERR))
                {
                    size_t user_idx = T_Vec_find_idx(state->users, (void*)curr_pollfd->fd, get_user_by_fd);
                    printf("Client disconnected in a funky way.\n");
                    User_remove(state->users, state->pollfds, state->channels, user_idx, x);
                }

                // The normal case, including when the other end disconnects unexpectedly
                // Build a "handle_params" struct for handle_msg and add it to worker
                // pool task queue
                if (test & (unsigned)POLLIN)
                {
                    //printf("Got message from fd: %d\n", params->clients->pollfds[x].fd);

                    // This allocation cannot be freed from here
                    // This thread will never know when the task has been picked up
                    // It shall be freed at the end of "handle_msg"
                    handle_message_args *h_params = calloc(1, sizeof(handle_message_args));
                    if (h_params == NULL)
                    {
                        perror("Failed to allocate h_params in notifier_thread.");
                        continue;
                    }

                    h_params->state = state;
                    h_params->incoming_socket = curr_pollfd->fd;

                    // Add task to the worker pools queue
                    Pool_addTask(state->workers, handle_msg, h_params);

                    // poll does not need to check this fd until it has been read from.
                    curr_pollfd->events = 0;
                }
            }
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s -p <port>\n", argv[0]);
        // Fancy return codes
        return EX_USAGE;
    }

    // Parse args
    optn *options = calloc(sizeof(optn), 1);
    if (options == NULL)
    {
        perror("Failed to allocate options struct in main.");
        return ENOMEM;
    }

    strncpy(options->port, "9997", 8);
    options->path = NULL;
    char opt;

    while ((opt = (char)getopt(argc, argv, "p:d:")) != -1)
    {
        switch (opt)
        {
            case 'p':
                //;
                //char *tmpPtr;
                //uint port = strtoul(optarg, &tmpPtr, 10);
                strncpy(options->port, optarg, 8);
                break;

            case 'd':
                options->path = calloc(1, PATH_MAX);
                if (options->path == NULL)
                {
                    perror("Failed to allocate options->path in main.");
                    free(options);
                    return ENOMEM;
                }
                strncpy(options->path, optarg, PATH_MAX);
                break;

            case '?':
                fprintf(stderr, "Usage: %s -p <port>\n", argv[0]);
                // Fancy return codes
                return EX_USAGE;

            default:
                break;

        }
    }

    if (options->path == NULL)
    {
        char *cwd = calloc(1, PATH_MAX);
        if (cwd == NULL)
        {
            perror("Failed to allocate cwd in main.");
            free(options);
            return ENOMEM;
        }
        options->path = getcwd(cwd, PATH_MAX);
        if (options->path == NULL)
        {
            perror("Invalid working directory...");
            free(options);
            free(cwd);
            return ENAMETOOLONG;
        }
    }

    struct addrinfo hints = {0};
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *results;
    int err = getaddrinfo(NULL, options->port, &hints, &results);
    if (err != 0)
    {
        fprintf(stderr, "Cannot get address: %s\n", gai_strerror(err));
        return EX_NOHOST;
    }

    int sd = socket(results->ai_family, results->ai_socktype, results->ai_protocol);
    if (sd < 0)
    {
        perror("Could not create socket");
        freeaddrinfo(results);
        return EX_OSERR;
    }
    err = bind(sd, results->ai_addr, results->ai_addrlen);

    if (err < 0)
    {
        perror("Could not bind socket");
        close(sd);
        freeaddrinfo(results);
        return EX_OSERR;
    }
    freeaddrinfo(results);

    // Backlog of 5 is typical
    err = listen(sd, 5);
    if (err < 0)
    {
        perror("Could not listen on socket");
        close(sd);
        return EX_OSERR;
    }
    fcntl(sd, O_NONBLOCK);

    // Set up the global state tracker
    global_state state;
    T_Vec *global_users = T_Vec_init(sizeof(user_info));
    T_Vec *global_channels = T_Vec_init(sizeof(channel_info));
    T_Vec *global_pollfds = T_Vec_init(sizeof(pollfd));

    state.users = global_users;
    state.channels = global_channels;
    state.pollfds = global_pollfds;
    state.sd = sd;

    // Create and lock this mutex, when its unlocked all
    // threads will cleanup and exit
    pthread_mutex_init(&state.mx, NULL);
    pthread_mutex_lock(&state.mx);

    // Initialize worker pool
    state.workers = Pool_create(4);

    // Create accepter thread
    pthread_t accepter;
    pthread_create(&accepter, NULL, accept_thread, &state);

    // Create notifier thread
    pthread_t notifier;
    pthread_create(&notifier, NULL, notifier_thread, &state);

    //Join the threads, they wont end until the "mx" mutex is unlocked
    pthread_join(notifier, NULL);
    printf("notifier joined\n");

    pthread_join(accepter, NULL);
    printf("accepter joined\n");

    Pool_wait(state.workers);

    //CLEANUP:
    printf("Server shutdown requested");
    T_Vec_destroy(state.users);
    T_Vec_destroy(state.channels);
    T_Vec_destroy(state.pollfds);

    free(options->path);
    free(options);

}