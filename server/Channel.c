//
// Created by charizard on 9/6/19.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//#include "User.h"
#include "Channel.h"

//T_Vec FindFunc function for matching by name
int find_channel_by_name(void *name, void *channel)
{
    char *n_name = name;
    channel_info *chan = channel;
    if (strncmp(name, chan->name, CHANNEL_NAME_LEN) == 0)
    {
        return 0;
    }
    return 1;
}


int
Channel_join(char *channel_name, T_Vec *channels, user_info *n_user)
{
    // find the channel
    channel_info * chan_to_join = T_Vec_find_ref(channels, channel_name, find_channel_by_name);

    // If it didnt exist, create it
    if (!chan_to_join)
    {
        int create = Channel_create(channel_name, channels);
        if (create == 1)
        {
            perror("Failed to join channel");
            return 1;
        }
        chan_to_join = T_Vec_find_ref(channels, channel_name, find_channel_by_name);
    }

    // Add the channel to the users channel list
    T_Vec_push(chan_to_join->users, n_user);
    T_Vec_push(n_user->channels, chan_to_join);
    return 0;

}

int
Channel_create(char *channelName, T_Vec *channels)
{
    channel_info new_channel;
    new_channel.users = T_Vec_init(sizeof(user_info));
    new_channel.motd = "MOTD IS CURRENTLY NOT SET";
    strncpy(new_channel.name, channelName, CHANNEL_NAME_LEN - 1);

    if (T_Vec_push(channels, &new_channel) == EXIT_FAILURE)
    {
        perror("Unable to create channel");
        return 1;
    }

    return 0;
}