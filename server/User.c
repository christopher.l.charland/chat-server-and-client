//
// Created by charizard on 9/3/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <string.h>
#include <unistd.h>

#include "User.h"

// Who wants to keep typing struct over and over?
typedef struct pollfd pollfd;

int
get_channel_by_name(void *arg_channel, void *arg_channel_name)
{
    channel_info *channel = arg_channel;
    char *channel_name = arg_channel_name;
    return strncmp(channel->name, channel_name, CHANNEL_NAME_LEN);
}

int
get_user_by_name(void* arg_user, void *arg_name)
{
    user_info *user = arg_user;
    char *name = arg_name;
    return strncmp(user->username, name, USERNAME_LEN);
}

int
User_remove(T_Vec *users, T_Vec *pollfds, T_Vec *channels, size_t user_idx, size_t pollfd_idx)
{
    // Get a reference to the user in question
    user_info *user = T_Vec_fetch_ref(users, user_idx);

    // While they still are in channels
    while (T_Vec_len(user->channels) > 0)
    {
        printf("In User_remove loop.\n");
        // Get the channel from the users list
        channel_info *channel = T_Vec_pop(user->channels);

        // Find that channel in the main list
        channel_info *channel_main = T_Vec_find_ref(channels, channel->name, get_channel_by_name);
        if (channel_main == NULL)
        {
            perror("channel_main is NULL in User_remove");
            return -1;
        }

        //Find the index of original user in that main channels user list
        ssize_t chan_user_idx = T_Vec_find_idx(channel_main->users, user->username, get_user_by_name);
        if (chan_user_idx == -1)
        {
            perror("chan_user_idx is -1 in User_remove");
            return -1;
        }

        // Remove the user from the channel
        T_Vec_remove(channel_main->users, chan_user_idx);

    }

    T_Vec_remove(users, user_idx);
    T_Vec_remove(pollfds, pollfd_idx);
    printf("User_remove called\n");


    return 0;
}

int
User_add(const char *username, T_Vec *users, T_Vec *pollfds, int fd)
{
    printf("User_add called\n");

    user_info user;
    user.sock_fd = fd;
    strncpy(user.username, username, USERNAME_LEN - 1);
    user.channels = T_Vec_init(sizeof(channel_info));

    pollfd n_pollfd;
    n_pollfd.fd = fd;
    n_pollfd.events = POLLIN;

    T_Vec_push(users, &user);
    T_Vec_push(pollfds, &n_pollfd);

    return 0;
}

int
User_destroy(user_list* users)
{
    free(users->pollfds);
    user* curr_user = users->userll;

    while (curr_user!=NULL) {
        user* prev = curr_user;
        curr_user = curr_user->next;
        free(prev);
    }
    free(users);
    return 0;
}
