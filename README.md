# CHAT SERVER AND CLIENT

Objective:  To build a chat server in C that supports multiple clients, whithin multiple rooms, as well as peer to peer file transfers. 
The chat server must be multi-threaded to support the clients needs.  

# Chat server/client requirements

* Each client must support the following operations:
    * Written in Python and use network sockets to communicate with a multi-threaded server.  
    * Register with the command "\register"
    * Join a channel
    * Send a message
    * Send a file to target user on the chat service
    * Receive a file from a user on the message
    * Ability to deny the receipt of a file
    * Ability to accept receipt of a file
    * Optional and only last thing (Consider using QT for building a interface)
* Server
    * Must be multi-threaded
    * must log all file transfers that occurred on the server (Name, time, Sender - Receiver, size,  Success or failure)
    * Must store all registered users and passwords in a log
    * All new registered users must be logged
    * Registered users will be required to log in with a password.
    * Users registering will be required to setup a password.
    * Must be written in C
    * Must transmit messages and files between clients appropriately
    * Must use a worker pool in regards to threading. Should be no more than 10 threads and a minimum of 3.
    * Must make good use of data structures
    * Stop Assuming success
* Documentation
    * Users' guide
    * Design Document
    * Test report
    * Doxygen Documentation of source code for developers
    * Commented code, all of it 
    * Ability to reproduce test by reading your test report (Include screen shots)
        * Expected Output vs. Expected Input given
* Development process
    * Must build with make
    * Must use good version control practices (i.e. Don't work on master)
    * Must have a debug and release build
    * All executables and binaries should be placed in a bin folder.
    * Project folder structure should be as follows:
```
    Chat Server
    |
    +---Documentation
    +---Binaries
    |   +---Chat client
    |   +---Chat server
    +---Src
        +---Makefile
        +---Server
        |   +---Includes
        |   +---Source Code
        +---Client
            +---All client code goes here
```   
